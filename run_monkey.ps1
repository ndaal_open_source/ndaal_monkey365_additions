param($ReportDir,
      $SubscriptionsFile,
      $TenantID)

Import-Module ./monkey365.psd1

Write-Host "Detecting platform..."
if($PSVersionTable.Platform -eq "Unix") {
  Write-Host "PowerShell Unix identified."
  $Platform = "Unix"
}
else {
  Write-Host "PowerShell Desktop identified."
  $Platform = "Windows"
}

if(!$SubscriptionsFile) {
  Write-Error "`nNo subscriptions file given. Cannot get information about specific subscriptions without knowing them..."
  exit 1
}

if(!$TenantID) {
  Write-Error "`nNo tenant ID given. Cannot get information about specific subscriptions without knowing the tenant..."
  exit 1
}

if($ReportDir) {
  Write-Host "`nUsing the given report directory."
  $ReportPath = $ReportDir
}
else {
  Write-Host "`nNo report directory is given. Using default one."
  if($Platform -eq "Unix") {
    $ReportPath = "$HOME/Monkey365"
  }
  else {
    $ReportPath = "C:\Monkey365\"
  }
}
Write-Host "Writing all results to: $ReportPath"

Write-Host "`nLoading subscriptions from $SubscriptionsFile"
$Subscriptions = Get-Content "$SubscriptionsFile"

Write-Host "`nRun Monkey365 for $($Subscriptions.Length) subscriptions.`n"
foreach($Subscription in $Subscriptions) {
  if($Platform -eq 'Unix') {
    $OutDir = "$ReportPath/$Subscription"
  }
  else {
    $OutDir = "$ReportPath\$Subscription"
  }
  $param = @{
    Instance = 'Azure';
    Analysis = 'All';
    PromptBehavior = 'SelectAccount';
    subscriptions = "$Subscription";
    TenantID = "$TenantID";
    ExportTo = 'HTML';
    OutDir = "$OutDir";
  }
  $assets = Invoke-Monkey365 @param
}
